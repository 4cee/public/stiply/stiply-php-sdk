<?php

namespace Stiply\Contracts\Config;

interface Config
{
    /**
     * Set the value for the given key.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function set(string $key, $value) : void;

    /**
     * Get the value for the given key.
     *
     * @param  string  $key
     * @return mixed
     */
    public function get(string $key);
}
