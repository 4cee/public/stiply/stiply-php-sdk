#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

# Install some base dependencies
apt-get update -yqq
apt-get install git libzip-dev zip -yqq

docker-php-ext-install zip

# Install Composer
curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install all project dependencies via Composer
composer install --ignore-platform-reqs --no-ansi --no-interaction --optimize-autoloader --no-suggest
